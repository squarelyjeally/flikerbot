from database.redis import db
import json


class UserManager:
    @staticmethod
    def prepare_data(chat_id: str):
        raw_data = {
            'retail': None,
            'fio': None,
            'phone': None,
            'code': None
        }
        db.set(chat_id, json.dumps(raw_data))

    @staticmethod
    def get_user_info(chat_id: str):
        return json.loads(db.get(chat_id))

    @staticmethod
    def update_user(chat_id: str, field, value):
        value = value.replace('/', '')
        new_dict = {field: value}
        existing_data = json.loads(db.get(chat_id))
        existing_data.update(new_dict)
        db.set(chat_id, json.dumps(existing_data))

    @staticmethod
    def clear_user_data(chat_id: str):
        db.delete(chat_id)

    @staticmethod
    def get_all_users():
        return db.keys()
