import redis
from confg import REDIS_HOST, REDIS_PORT

db = redis.Redis(host=REDIS_HOST, port=REDIS_PORT)
