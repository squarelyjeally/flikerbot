
import csv
from os import path

from const import PRODUCTS, RETAIL_MAP, TTT_RETAIL
from users.user_manager import UserManager
from synchronizer import DocSynchronizer


class UserStepProcessor:
    def __init__(self, step_value):
        self.step = step_value

    def process_step(self, message_object, **kwargs):
        callback_message = None
        valid = True
        user_data = UserManager.get_user_info(str(message_object.chat.id))

        if self.step == 1:
            retail = RETAIL_MAP[message_object.text]
            UserManager.update_user(str(message_object.chat.id), 'retail',
                                    retail)
            callback_message = 'Введите ФИО: \n(Пример: Иванов Иван Петрович)'
        elif self.step == 2 and user_data['retail'] != TTT_RETAIL:
            UserManager.update_user(str(message_object.chat.id), 'fio',
                                    message_object.text)
            if user_data['retail'] != TTT_RETAIL:
                callback_message = 'Введите код сотрудника: \n(Пример: 00777)'
        elif self.step == 3 or self.step == 2 and user_data['retail'] == \
                TTT_RETAIL:
            if user_data['retail'] != TTT_RETAIL:
                if message_object.text.isnumeric():
                    UserManager.update_user(str(message_object.chat.id),
                                            'code', message_object.text)
                    callback_message = 'Введите номер мобильного телефона:' \
                                       ' \n(Пример: 0667778899)'
                else:
                    callback_message = 'Вы ввели не код сотрудника'
                    valid = False
            else:
                UserManager.update_user(str(message_object.chat.id), 'fio',
                                        message_object.text)
                callback_message = 'Введите номер мобильного телефона:' \
                                   ' \n(Пример: 0667778899)'
        elif self.step == 4:
            user_info = UserManager.get_user_info(str(message_object.chat.id))
            if user_info['retail'] == TTT_RETAIL:
                UserManager.update_user(str(message_object.chat.id), 'code',
                                        message_object.text)
            UserManager.update_user(str(message_object.chat.id), 'phone',
                                    message_object.text)
            user_info = UserManager.get_user_info(str(message_object.chat.id))
            user_info.update({'id': str(message_object.chat.id),
                             'username': message_object.from_user.username})
            DocSynchronizer().add_new_user_to_googlesheets_file(user_info)

            return 'Поздравляю, Вы в Мотивационной Программе! Теперь Вы ' \
                   'можете отправить ключ продукта. ', valid
        elif self.step == 5:
            user_info = kwargs.get('user_info')
            file_info = list()
            file_info.append(user_info['retail'])
            file_info.append(user_info['fio'])
            file_info.append(user_info['code'])
            file_info.append(user_info['phone'])
            file_info.append(message_object.text)
            if not path.isfile(PRODUCTS):
                with open(PRODUCTS, 'a') as new_csv_file:
                    writer = csv.writer(new_csv_file, delimiter=',')
                    writer.writerow(['Ритейл', 'ФИО',
                                     'КодСотрудника', 'ТелНомер',
                                     'КодПродукта'])
            with open(PRODUCTS, 'a', encoding='utf-8') as csvfile:
                writer = csv.writer(csvfile, delimiter=',')
                writer.writerow(file_info)
            DocSynchronizer().append_to_googlesheets_file(user_info,
                                                          message_object.text)
            return 'Ключ успешно зарегистрирован! Жду следующий 😉', valid

        return callback_message, valid

