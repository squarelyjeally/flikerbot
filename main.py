from os.path import isfile

import telebot

from const import TOKEN, VERY_FIRST_STEP, FINAL_STEP, START_COMMAND, \
    REGISTER_COMMAND, \
    GET_FILE_COMMAND, PRODUCTS, \
    STOP_COMMAND, DEREGISTER_COMMAND, MY_RESULT_LINK, SEND_TO_ALL_COMMAND, \
    ALWAYS_ON_KEYS, RETAIL_COMMANDS, ALLO_RETAIL_ENG, TTT_RETAIL_ENG, \
    MORE_INFORMATION_BUTTON, MY_RESULT_BUTTON, \
    MORE_INFORMATION_LINK, RINGOO_RETAIL_ENG, CCM_RETAIL_ENG, FISHKI_RETAIL_ENG, TOUCH_RETAIL_ENG,\
    SMARTS_RETAIL_ENG, SMARTSHOP_RETAIL_ENG
from users.user_manager import UserManager
from utils import check_user, process_current_step, check_user_retail


if __name__ == '__main__':
    bot = telebot.TeleBot(TOKEN, threaded=False)
    types = telebot.types
    current_step_value = VERY_FIRST_STEP


    def get_always_markup():
        markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
        markup.row(*[types.KeyboardButton(el) for el in ALWAYS_ON_KEYS])
        return markup


    @bot.message_handler(commands=[START_COMMAND])
    def process_start(message):
        print('start from {}'.format(message.from_user))
        status, user_data = check_user(message)
        if not status:
            bot.send_message(message.chat.id, "Привет! Зарегистрируйтесь,"
                                              " прежде чем начать работу. "
                                              "Для этого введите команду"
                                              " /{}.".format(REGISTER_COMMAND))


    @bot.message_handler(commands=[REGISTER_COMMAND])
    def process_registration(message):
        global current_step_value
        print('reg from {}'.format(message.from_user))
        status, user_data = check_user(message)
        if status:
            bot.send_message(message.chat.id,
                             "ФИО - {}\nРитейл - {}\nНомер телефона - "
                             "{}\nКод сотрудника - {}.\nЧтобы изменить "
                             "регистрационные данные, отмените регистрацию с "
                             "помощью команды  /{}, и зарегистрируйтесь "
                             "заново.".format(user_data['fio'],
                                              user_data['retail'],
                                              user_data['phone'],
                                              user_data['code'],
                                              DEREGISTER_COMMAND))
        else:
            current_step_value = VERY_FIRST_STEP + 1
            UserManager.prepare_data(str(message.chat.id))
            bot.send_message(message.chat.id,
                             "Выберите один из доступных ритейлов"
                             " используя команду:\n- /{}\n- /{}\n- /{}\n- /{}\n- /{}\n- "
                             "/{}\n- /{}\n- /{}".format(
                                 ALLO_RETAIL_ENG, TTT_RETAIL_ENG, RINGOO_RETAIL_ENG,
                                 SMARTS_RETAIL_ENG,
                                 CCM_RETAIL_ENG, FISHKI_RETAIL_ENG, TOUCH_RETAIL_ENG,
                                 SMARTSHOP_RETAIL_ENG))


    @bot.message_handler(commands=[DEREGISTER_COMMAND])
    def process_deregister(message):
        print('dereg from {}'.format(message.from_user))
        global current_step_value
        current_step_value = 0
        UserManager.clear_user_data(str(message.chat.id))
        bot.send_message(message.chat.id, "Чтобы ввести регистрационные "
                                          "данные введите команду /{}.".format(
            REGISTER_COMMAND))


    @bot.message_handler(commands=[GET_FILE_COMMAND])
    def process_get_file(message):
        print('download from {}'.format(message.from_user))
        if isfile(PRODUCTS):
            with open(PRODUCTS, 'rb') as csv_file:
                bot.send_document(message.chat.id, csv_file)


    @bot.message_handler(commands=[STOP_COMMAND])
    def process_stop(message):
        print('stop from {}'.format(message.from_user))
        bot.send_message(message.chat.id, 'Ok.')


    @bot.message_handler(commands=[SEND_TO_ALL_COMMAND])
    def process_my_result(message):
        print('sending to all from {}'.format(message.from_user))
        for key in UserManager.get_all_users():
            try:
                bot.send_message(key.decode(), message.text.replace(
                    '/{}'.format(SEND_TO_ALL_COMMAND), ''))
            except Exception as e:
                print('during all notify {}'.format(e))


    @bot.message_handler(commands=RETAIL_COMMANDS)
    def retail_process(message):
        global current_step_value
        status = check_user_retail(message)
        print('retail setting from {} {}'.format(message.from_user,
                                                 message.text))
        if not status and current_step_value > VERY_FIRST_STEP:
            callback_message, new_step_value = process_current_step(
                current_step_value, message)
            if message.text == '/{}'.format(TTT_RETAIL_ENG):
                new_step_value += 1
            current_step_value = new_step_value
            bot.send_message(message.chat.id, callback_message)
        elif status:
            bot.send_message(message.chat.id, 'Вы уже выбрали необходимый '
                                              'ритейл. Чтобы очистить свои'
                                              ' данные введите команду '
                                              '/{}'.format(DEREGISTER_COMMAND))
        else:
            current_step_value = VERY_FIRST_STEP
            bot.send_message(message.chat.id, 'Прежде чем приступить к работе,'
                                              ' необходимо зарегистрироваться. '
                                              'Для этого введите команду '
                                              '/{}'.format(REGISTER_COMMAND))


    @bot.message_handler(content_types=['text'])
    def text_process(message):
        global current_step_value
        status, user_data = check_user(message)
        print('processing message from {} {}'.format(message.from_user,
                                                     message.text))
        if status:
            print('STATUS FUCKED')
            if message.text == MORE_INFORMATION_BUTTON:
                bot.send_message(message.chat.id, MORE_INFORMATION_LINK)
            elif message.text == MY_RESULT_BUTTON:
                bot.send_message(message.chat.id, MY_RESULT_LINK)
            elif len(message.text) in [12, 16, 7, 26]:
                current_step_value = FINAL_STEP
                callback_message, new_step_value = process_current_step(
                    current_step_value, message, user_info=user_data)
                current_step_value = new_step_value
                bot.send_message(message.chat.id, callback_message,
                                 reply_markup=get_always_markup())
            else:
                bot.reply_to(message, 'Вы ввели не ключ. '
                                      'Пожалуйста введите ключ от продукта.')
        elif current_step_value > VERY_FIRST_STEP:
            print('ELIF FUCKED')
            if isinstance(user_data, str):
                bot.send_message(message.chat.id,
                                 'Выберите правильно {}.'.format(user_data))
            markup = None
            callback_message, new_step_value = process_current_step(
                current_step_value, message)
            current_step_value = new_step_value
            if new_step_value == FINAL_STEP:
                markup = get_always_markup()
            bot.send_message(message.chat.id, callback_message,
                             reply_markup=markup)
        else:
            current_step_value = VERY_FIRST_STEP
            bot.send_message(message.chat.id, 'Прежде чем приступить к работе,'
                                              ' необходимо зарегистрироваться.'
                                              '  Для этого введите команду '
                                              '/{}'.format(REGISTER_COMMAND))


    while True:
        try:
            print('start')
            bot.polling(none_stop=True)
        except Exception as e:
            bot.stop_polling()
            print(e)
