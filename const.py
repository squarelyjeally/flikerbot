TOKEN = '663451453:AAET0_qYgFdOdVepJp0DwvWj5bHw9ip3ia0'

ALLO_RETAIL = 'Алло'
ALLO_RETAIL_ENG = 'allo'

TTT_RETAIL_ENG = 'ttt'
TTT_RETAIL = 'ТТТ'

CCM_RETAIL = 'ССМ-Тел'
CCM_RETAIL_ENG = 'ccmtel'

FISHKI_RETAIL = 'fishki.ua'
FISHKI_RETAIL_ENG = 'fishki'

TOUCH_RETAIL = 'touch'
TOUCH_RETAIL_ENG = 'touch'

RINGOO_RETAIL = 'Ringoo'
RINGOO_RETAIL_ENG = 'ringoo'

SMARTS_RETAIL = 'Smarts'
SMARTS_RETAIL_ENG = 'smarts'

SMARTSHOP_RETAIL = 'Smartshop'
SMARTSHOP_RETAIL_ENG = 'smartshop'

RETAIL_NETS = [ALLO_RETAIL, TTT_RETAIL]
RETAIL_COMMANDS = [ALLO_RETAIL, TTT_RETAIL_ENG, RINGOO_RETAIL_ENG, CCM_RETAIL_ENG,
                   FISHKI_RETAIL_ENG, TOUCH_RETAIL_ENG, SMARTSHOP_RETAIL_ENG, SMARTS_RETAIL_ENG]
RETAIL_MAP = {'/{}'.format(TTT_RETAIL_ENG): TTT_RETAIL,
              '/{}'.format(ALLO_RETAIL_ENG): ALLO_RETAIL,
              '/{}'.format(SMARTS_RETAIL_ENG): SMARTS_RETAIL,
              '/{}'.format(SMARTSHOP_RETAIL_ENG): SMARTSHOP_RETAIL,
              '/{}'.format(RINGOO_RETAIL_ENG): RINGOO_RETAIL,
              '/{}'.format(CCM_RETAIL_ENG): CCM_RETAIL,
              '/{}'.format(FISHKI_RETAIL_ENG): FISHKI_RETAIL,
              '/{}'.format(TOUCH_RETAIL_ENG): TOUCH_RETAIL}


FIELD_VALUES = {'retail': 'ритейл',
                'fio': 'ФИО',
                'phone': 'номер телефона',
                'code': 'код сотрудника'}

MORE_INFORMATION_BUTTON = 'Больше информации о мотивационной программе и плюшках'
MY_RESULT_BUTTON = 'Мои результаты'
ALWAYS_ON_KEYS = [MORE_INFORMATION_BUTTON, MY_RESULT_BUTTON]

START_COMMAND = 'start'
REGISTER_COMMAND = 'register'
DEREGISTER_COMMAND = 'cancel_registration'
GET_FILE_COMMAND = 'give_me_file' # secret
SEND_TO_ALL_COMMAND = 'notify_all' # secret
STOP_COMMAND = 'stop'

PRODUCTS = 'products.csv'
USERS = 'users'

VERY_FIRST_STEP = 0
FINAL_STEP = 5


MY_RESULT_LINK = "https://docs.google.com/spreadsheets/d/1_x0Gj3HiMcPaGDbOVkvmHsvsTzHvGWPSFFGN0n9HSCA/"
MORE_INFORMATION_LINK = "https://fliker.com.ua/gift.html"

SCOPES = ['https://www.googleapis.com/auth/spreadsheets']
SPREADSHEET_ID = '1WULvF83-awO90ysv7I4ls-3iKGjWbSOkIU-a6l9p9Aw'
