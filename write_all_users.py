from users.user_manager import UserManager
from synchronizer import DocSynchronizer


all_users = UserManager.get_all_users()

for user_chat_id in all_users:
    user_info = UserManager.get_user_info(user_chat_id)
    user_info.update({'id': user_chat_id.decode(),
                      'username': '***'})
    DocSynchronizer().add_new_user_to_googlesheets_file(user_info)
