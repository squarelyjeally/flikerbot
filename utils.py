from const import FIELD_VALUES, TTT_RETAIL, ALLO_RETAIL
from database.redis import db
from steps import UserStepProcessor
from users.user_manager import UserManager


def check_user(message):
    if db.exists(str(message.chat.id)):
        user_data = UserManager.get_user_info(str(message.chat.id))
        for k, v in user_data.items():
            if v is None:
                if k == 'code' and user_data['retail'] == TTT_RETAIL:
                    continue
                elif k == 'retail':
                    return False, FIELD_VALUES[k]
                else:
                    return False, user_data
        user_data.update({'id': str(message.chat.id),
                          'username': message.from_user.username})
        return True, user_data
    return False, None


def process_current_step(step, message, **kwargs):
    return_message, valid = UserStepProcessor(step).process_step(message,
                                                                 **kwargs)
    if valid:
        return return_message, step + 1
    else:
        return return_message, step


def check_user_retail(message):
    if db.exists(str(message.chat.id)):
        user_data = UserManager.get_user_info(str(message.chat.id))
        if user_data.get('retail') is not None:
            return True
    return False
