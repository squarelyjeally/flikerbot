import pickle
from datetime import datetime

from google.auth.transport.requests import Request
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build

from const import SCOPES, SPREADSHEET_ID


class DocSynchronizer:
    def __init__(self):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)

        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    'cred.json', SCOPES)
                creds = flow.run_local_server()

            with open('token.pickle', 'wb') as token:
                pickle.dump(creds, token)

        self.service = build('sheets', 'v4', credentials=creds)

    def append_to_googlesheets_file(self, user_data: dict, key: str):
        values = list()
        values.append(datetime.now().strftime("%d.%m.%Y %H:%M:%S"))
        values.append(user_data['retail'])
        values.append('')
        values.append(key)
        values.append(user_data['fio'])
        values.append(user_data['code'])
        values.append(user_data['username'])
        values.append(user_data['id'])
        sheet = self.service.spreadsheets().values()

        sheet.append(spreadsheetId=SPREADSHEET_ID, range='A1',
                     body={'values': [values]},
                     valueInputOption='USER_ENTERED').execute()

    def add_new_user_to_googlesheets_file(self, user_data: dict):
        values = list()
        values.append(user_data['fio'])
        values.append(user_data['retail'])
        values.append(user_data['code'])
        values.append(user_data['username'])
        values.append(user_data['id'])
        sheet = self.service.spreadsheets().values()
        print(values)
        sheet.append(spreadsheetId=SPREADSHEET_ID, range='Пользователи',
                     body={'values': [values]},
                     valueInputOption='USER_ENTERED').execute()
